﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGPIP2
{
    public class ImageProperties
    {
        public static List<CGPImage> GetProperties(CGPImage Src)
        {
            Console.WriteLine("Getting properties....");
            
            List<CGPImage> Properties = new List<CGPImage>();
            int[] Apertures = Parameters.ImagePropertyApertures;

            foreach (int Aperture in Apertures)
            {
                Console.WriteLine("\twith aperture " + Aperture);
                Properties.Add(CGPImageFunctions.LocalNormalize(Src, Aperture));
                //Properties.Add(CGPImageFunctions.CompareLocalStatsToGlobal(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Min));
                //Properties.Add(CGPImageFunctions.CompareLocalStatsToGlobal(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Max));
                //Properties.Add(CGPImageFunctions.CompareLocalStatsToGlobal(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Range));
                Properties.Add(CGPImageFunctions.LocalStats(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Min));
                Properties.Add(CGPImageFunctions.LocalStats(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Max));
                Properties.Add(CGPImageFunctions.LocalStats(Src, Aperture, Aperture, CGPImageFunctions.LocalStat.Range));
                
            }
            Properties.Add(CGPImageFunctions.LocalNormalizeWithGauss(Src, 30, 8));

            return Properties;
        }
    }
}
