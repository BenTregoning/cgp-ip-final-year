﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGPIP2
{
    public enum CGPFunction
    {
        CONST,
        ADD,
        ADDConst,
        SUBTRACT,
        SUBTRACTConst,
        DIVIDE,
        MULTIPLY,
        MULTIPLYConst,
        NOP,
        SHIFTUP,
        SHIFTDOWN,
        SHIFTLEFT,
        SHIFTRIGHT,
        ERODE,
        DILATE,
        THRESHOLD,
        THRESHOLDINV,
        OPEN,
        CLOSE,
        TOPHAT,
        BLACKHAT,
        GRADIENT,
        ADAPTIVETHRESHOLD,
        RESIZE,
        SMOOTHBILATRAL,
        MIN,
        MAX,
        MINConst,
        MAXConst,
#if GABOR
        GABOR,
#endif
        ABSDIFF,
        CANNY,
        LAPLACE,
        LOG,
        SQRT,
        SOBEL,
        SCALE,
        EQ,
        MINVALUE,
        MAXVALUE,
        RESAMPLE,
        FEATURE0,
        FEATURE1,
        FEATURE2,
        FEATURE3,
        FEATURE4,
        FEATURE5,
        SMOOTHMEDIAN,
        SMOOTHGAUSSIAN,
        SHIFT,
        COMP,
        COMP2,
        ADD3,
        AVG,
        AND,
        OR,
        XOR,
        NOT,
        POW,
        UNSHARPEN,
        ROTATE,
        LMIN,
        LMAX,
        LAVERAGE,
        LRANGE,
        LCMPMIN,
        LCMPMAX,
        LCMPAVERAGE,
        LCMPRANGE,
        MEDIAN,
        SMOOTH,
        GrayLevelFeature,
        NORMGUASS,
        ABS,
        ABSTHRESHOLD,
        OTSU,
        CONVOLVE,
        EDGEANGLE,
        EDGEMAG,
        QUANTIZE,
        SETHIGH,
        SETLOW,
        Ra, Rq, Rv, Rp, Rt, Rsk, Rku, Rz, Rent,
        LMAJ
    }

    public class FunctionSetDefinition
    {
        public static CGPFunction[] MapCGPFunctions =
      {
         CGPFunction.CONST,
         CGPFunction.ADDConst,
         CGPFunction.SUBTRACTConst,
         CGPFunction.MULTIPLYConst,
         CGPFunction.ADD,
         CGPFunction.SUBTRACT,
         CGPFunction.MULTIPLY,
         CGPFunction.DIVIDE,
         CGPFunction.SHIFT,
         CGPFunction.SHIFTUP,
         CGPFunction.SHIFTDOWN,
         CGPFunction.SHIFTLEFT,
         CGPFunction.SHIFTRIGHT,
         CGPFunction.ERODE,
         CGPFunction.DILATE,
         CGPFunction.THRESHOLD,
         CGPFunction.THRESHOLDINV,
         CGPFunction.OPEN,
         CGPFunction.CLOSE,
         CGPFunction.TOPHAT,
         CGPFunction.BLACKHAT,
         CGPFunction.GRADIENT,
         CGPFunction.ADAPTIVETHRESHOLD,  
         CGPFunction.MIN,
      CGPFunction.MAX,
      CGPFunction.MINConst,
      CGPFunction.MAXConst,
      #if GABOR

      CGPFunction.GABOR,    
#endif
        CGPFunction.CANNY,       
       CGPFunction.LAPLACE,
        CGPFunction.LOG,
       CGPFunction.SQRT,
       CGPFunction.SOBEL,
        CGPFunction.MINVALUE,
        CGPFunction.MAXVALUE,        
        CGPFunction.SMOOTHMEDIAN,
        CGPFunction.SMOOTHGAUSSIAN,        
        CGPFunction.ADD3,
        CGPFunction.AVG,
        CGPFunction.POW,
        CGPFunction.UNSHARPEN,
      //  CGPFunction.GrayLevelFeature,
        CGPFunction.CONVOLVE,
        CGPFunction.Ra, 
        CGPFunction.Rq, 
        CGPFunction.Rv, 
        CGPFunction.Rp, 
        CGPFunction.Rt, 
        CGPFunction.Rsk, 
        CGPFunction.Rku, 
        CGPFunction.Rz,
        CGPFunction.Rent,
      };


        public static CGPFunction[] CGPFunctions =
      {
         CGPFunction.CONST,
         CGPFunction.ADDConst,
         CGPFunction.SUBTRACTConst,
         CGPFunction.MULTIPLYConst,
         CGPFunction.ADD,
         CGPFunction.SUBTRACT,
         CGPFunction.MULTIPLY,
         CGPFunction.DIVIDE,
         CGPFunction.SHIFT,
         CGPFunction.SHIFTUP,
         CGPFunction.SHIFTDOWN,
         CGPFunction.SHIFTLEFT,
         CGPFunction.SHIFTRIGHT,
         CGPFunction.ERODE,
         CGPFunction.DILATE,
         CGPFunction.THRESHOLD,
         CGPFunction.THRESHOLDINV,
       CGPFunction.OPEN,
       CGPFunction.CLOSE,
       CGPFunction.TOPHAT,
       CGPFunction.BLACKHAT,
       CGPFunction.GRADIENT,
         CGPFunction.ADAPTIVETHRESHOLD,
         CGPFunction.RESIZE,
         CGPFunction.SMOOTHBILATRAL,
         CGPFunction.MIN,
      CGPFunction.MAX,
      CGPFunction.MINConst,
      CGPFunction.MAXConst,
#if GABOR
      CGPFunction.GABOR,
      #endif
      CGPFunction.ABSDIFF,
        CGPFunction.CANNY,       
       CGPFunction.LAPLACE,
        CGPFunction.LOG,
       CGPFunction.SQRT,
       CGPFunction.SOBEL,
    //   CGPFunction.SCALE,
       CGPFunction.EQ,
        CGPFunction.MINVALUE,
        CGPFunction.MAXVALUE,
        CGPFunction.RESAMPLE,
//CGPFunction.FEATURE0,
//CGPFunction.FEATURE1,
//CGPFunction.FEATURE2,
//CGPFunction.FEATURE3,
//CGPFunction.FEATURE4,
//CGPFunction.FEATURE5,
        CGPFunction.SMOOTHMEDIAN,
        CGPFunction.SMOOTHGAUSSIAN,
        CGPFunction.COMP,
        CGPFunction.COMP2,
        CGPFunction.ADD3,
        CGPFunction.AVG,
        CGPFunction.AND,
        CGPFunction.OR,
        CGPFunction.XOR,
        CGPFunction.NOT,
        CGPFunction.POW,
        CGPFunction.UNSHARPEN,
       // CGPFunction.ROTATE,
//CGPFunction.LMIN,
//CGPFunction.LMAX,
//CGPFunction.LAVERAGE,
//CGPFunction.LRANGE,
//CGPFunction.LCMPMIN,
//CGPFunction.LCMPMAX,
//CGPFunction.LCMPAVERAGE,
//CGPFunction.LCMPRANGE,
        CGPFunction.SMOOTH,
     //CGPFunction.GrayLevelFeature,
     CGPFunction.NORMGUASS,
     CGPFunction.ABS,
        CGPFunction.ABSTHRESHOLD,
        CGPFunction.OTSU,
                CGPFunction.CONVOLVE,
                CGPFunction.UNSHARPEN,
                CGPFunction.EDGEANGLE,
        CGPFunction.EDGEMAG,
        CGPFunction.QUANTIZE,
        //CGPFunction.SETHIGH,
        //CGPFunction.SETLOW,
        CGPFunction.Ra, 
        CGPFunction.Rq, 
        CGPFunction.Rv, 
        CGPFunction.Rp, 
        CGPFunction.Rt, 
        CGPFunction.Rsk, 
        CGPFunction.Rku, 
        CGPFunction.Rz,
        CGPFunction.Rent,
        CGPFunction.LMAJ
        
      };

        public static CGPFunction RandomFunction()
        {
            return CGPFunctions[FastRandom.Rng.Next(0, CGPFunctions.Length)];
        }

        public static CGPFunction RandomFunction(int Arity)
        {
            List<CGPFunction> FunctionsOfArity = CGPFunctionsByArity[Arity];

            return FunctionsOfArity[FastRandom.Rng.Next(0, FunctionsOfArity.Count)];
        }

        public static Dictionary<CGPFunction, int> CGPFunctionArity = new Dictionary<CGPFunction, int>();
        public static Dictionary<int, List<CGPFunction>> CGPFunctionsByArity = new Dictionary<int, List<CGPFunction>>();

        public static void ConfigureArity()
        {
            CGPFunctionArity.Add(CGPFunction.CONST, 0);
            CGPFunctionArity.Add(CGPFunction.NOP, 1);

            CGPFunctionArity.Add(CGPFunction.ADD, 2);
            CGPFunctionArity.Add(CGPFunction.SUBTRACT, 2);
            CGPFunctionArity.Add(CGPFunction.DIVIDE, 2);
            CGPFunctionArity.Add(CGPFunction.MULTIPLY, 2);

            CGPFunctionArity.Add(CGPFunction.ADDConst, 1);
            CGPFunctionArity.Add(CGPFunction.SUBTRACTConst, 1);
            CGPFunctionArity.Add(CGPFunction.MULTIPLYConst, 1);

            CGPFunctionArity.Add(CGPFunction.SHIFT, 1);
            CGPFunctionArity.Add(CGPFunction.SHIFTUP, 1);
            CGPFunctionArity.Add(CGPFunction.SHIFTDOWN, 1);
            CGPFunctionArity.Add(CGPFunction.SHIFTLEFT, 1);
            CGPFunctionArity.Add(CGPFunction.SHIFTRIGHT, 1);

            CGPFunctionArity.Add(CGPFunction.ERODE, 1);
            CGPFunctionArity.Add(CGPFunction.DILATE, 1);

            CGPFunctionArity.Add(CGPFunction.THRESHOLD, 1);
            CGPFunctionArity.Add(CGPFunction.THRESHOLDINV, 1);

            CGPFunctionArity.Add(CGPFunction.OPEN, 1);
            CGPFunctionArity.Add(CGPFunction.CLOSE, 1);
            CGPFunctionArity.Add(CGPFunction.TOPHAT, 1);
            CGPFunctionArity.Add(CGPFunction.BLACKHAT, 1);
            CGPFunctionArity.Add(CGPFunction.GRADIENT, 1);

            CGPFunctionArity.Add(CGPFunction.ADAPTIVETHRESHOLD, 1);

            CGPFunctionArity.Add(CGPFunction.RESIZE, 1);
            CGPFunctionArity.Add(CGPFunction.SMOOTHBILATRAL, 1);

            CGPFunctionArity.Add(CGPFunction.MIN, 2);
            CGPFunctionArity.Add(CGPFunction.MAX, 2);
            CGPFunctionArity.Add(CGPFunction.MINConst, 1);
            CGPFunctionArity.Add(CGPFunction.MAXConst, 1);

#if GABOR
            CGPFunctionArity.Add(CGPFunction.GABOR, 1);
#endif

            CGPFunctionArity.Add(CGPFunction.ABSDIFF, 2);
            CGPFunctionArity.Add(CGPFunction.CANNY, 1);
            CGPFunctionArity.Add(CGPFunction.LAPLACE, 1);
            CGPFunctionArity.Add(CGPFunction.LOG, 1);
            CGPFunctionArity.Add(CGPFunction.SQRT, 1);

            CGPFunctionArity.Add(CGPFunction.SOBEL, 1);

            CGPFunctionArity.Add(CGPFunction.SCALE, 1);
            CGPFunctionArity.Add(CGPFunction.RESAMPLE, 1);
            CGPFunctionArity.Add(CGPFunction.EQ, 1);

            CGPFunctionArity.Add(CGPFunction.MINVALUE, 1);
            CGPFunctionArity.Add(CGPFunction.MAXVALUE, 1);

            CGPFunctionArity.Add(CGPFunction.FEATURE0, 1);
            CGPFunctionArity.Add(CGPFunction.FEATURE1, 1);
            CGPFunctionArity.Add(CGPFunction.FEATURE2, 1);
            CGPFunctionArity.Add(CGPFunction.FEATURE3, 1);
            CGPFunctionArity.Add(CGPFunction.FEATURE4, 1);
            CGPFunctionArity.Add(CGPFunction.FEATURE5, 1);

            CGPFunctionArity.Add(CGPFunction.SMOOTHMEDIAN, 1);
            CGPFunctionArity.Add(CGPFunction.SMOOTHGAUSSIAN, 1);

            CGPFunctionArity.Add(CGPFunction.COMP, 3);
            CGPFunctionArity.Add(CGPFunction.COMP2, 3);

            CGPFunctionArity.Add(CGPFunction.ADD3, 3);
            CGPFunctionArity.Add(CGPFunction.AVG, 3);

            CGPFunctionArity.Add(CGPFunction.AND, 2);
            CGPFunctionArity.Add(CGPFunction.OR, 2);
            CGPFunctionArity.Add(CGPFunction.XOR, 2);
            CGPFunctionArity.Add(CGPFunction.NOT, 1);
            CGPFunctionArity.Add(CGPFunction.POW, 1);

            CGPFunctionArity.Add(CGPFunction.UNSHARPEN, 1);
            CGPFunctionArity.Add(CGPFunction.ROTATE, 1);

            CGPFunctionArity.Add(CGPFunction.LMIN, 1);
            CGPFunctionArity.Add(CGPFunction.LMAX, 1);
            CGPFunctionArity.Add(CGPFunction.LAVERAGE, 1);
            CGPFunctionArity.Add(CGPFunction.LRANGE, 1);

            CGPFunctionArity.Add(CGPFunction.LCMPMIN, 1);
            CGPFunctionArity.Add(CGPFunction.LCMPMAX, 1);
            CGPFunctionArity.Add(CGPFunction.LCMPAVERAGE, 1);
            CGPFunctionArity.Add(CGPFunction.LCMPRANGE, 1);


            CGPFunctionArity.Add(CGPFunction.SMOOTH, 1);

            CGPFunctionArity.Add(CGPFunction.GrayLevelFeature, 1);

            CGPFunctionArity.Add(CGPFunction.NORMGUASS, 1);

            CGPFunctionArity.Add(CGPFunction.ABS, 1);
            CGPFunctionArity.Add(CGPFunction.ABSTHRESHOLD, 1);
            CGPFunctionArity.Add(CGPFunction.OTSU, 1);

            CGPFunctionArity.Add(CGPFunction.CONVOLVE, 1);


            CGPFunctionArity.Add(CGPFunction.EDGEANGLE, 1);
            CGPFunctionArity.Add(CGPFunction.EDGEMAG, 1);

            CGPFunctionArity.Add(CGPFunction.QUANTIZE, 1);

            CGPFunctionArity.Add(CGPFunction.SETHIGH, 1);
            CGPFunctionArity.Add(CGPFunction.SETLOW, 1);

            CGPFunctionArity.Add(CGPFunction.Ra, 1);
            CGPFunctionArity.Add(CGPFunction.Rq, 1);
            CGPFunctionArity.Add(CGPFunction.Rv, 1);
            CGPFunctionArity.Add(CGPFunction.Rp, 1);
            CGPFunctionArity.Add(CGPFunction.Rt, 1);
            CGPFunctionArity.Add(CGPFunction.Rsk, 1);
            CGPFunctionArity.Add(CGPFunction.Rku, 1);
            CGPFunctionArity.Add(CGPFunction.Rz, 1);
            CGPFunctionArity.Add(CGPFunction.Rent, 1);

            CGPFunctionArity.Add(CGPFunction.LMAJ, 1);

            foreach (CGPFunction F in CGPFunctionArity.Keys)
            {
                int Arity = CGPFunctionArity[F];
                if (!CGPFunctionsByArity.ContainsKey(Arity))
                {
                    CGPFunctionsByArity.Add(Arity, new List<CGPFunction>());
                }
                CGPFunctionsByArity[Arity].Add(F);
            }
        }

        static FunctionSetDefinition()
        {
            ConfigureArity();
        }
    }
}