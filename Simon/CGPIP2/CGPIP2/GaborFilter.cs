﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Img = Emgu.CV.Image<Emgu.CV.Structure.Gray, float>;

namespace CGPIP2
{
   public class GaborKernel
   {
      public ConvolutionKernelF KernelRealData;
      public ConvolutionKernelF KernelImgData;
      private int GaborWidth = 9;
      public int GaborHeight = 9;
      public int orientation = 8;
      public int frequency = 3;

      private void CalculateKernel(int Orientation, int Frequency, int GaborWidth, int GaborHeight)
      {
         if (GaborWidth < 1) GaborWidth = 1;
         if (GaborHeight < 1) GaborHeight = 1;
         this.GaborHeight = GaborHeight;
         this.GaborWidth = GaborWidth;
         this.orientation = Orientation;
         this.frequency = Frequency;

         this.KernelRealData = new ConvolutionKernelF(GaborWidth, GaborHeight);
         this.KernelImgData = new ConvolutionKernelF(GaborWidth, GaborHeight);

         float real, img;

         for (int x = -(GaborWidth - 1) / 2; x < (GaborWidth - 1) / 2 + 1; x++)
            for (int y = -(GaborHeight - 1) / 2; y < (GaborHeight - 1) / 2 + 1; y++)
            {
               real = KernelRealPart(x, y, Orientation, Frequency);
               img = KernelImgPart(x, y, Orientation, Frequency);

               KernelRealData[x + (GaborWidth - 1) / 2, y + (GaborHeight - 1) / 2] = real;
               KernelImgData[x + (GaborWidth - 1) / 2, y + (GaborHeight - 1) / 2] = img;
            }
      }

      private float KernelRealPart(int x, int y, int Orientation, int Frequency)
      {
         double U, V;
         double Sigma, Kv, Qu;
         double tmp1, tmp2;

         U = Orientation;
         V = Frequency;
         Sigma = 2 * Math.PI * Math.PI;
         Kv = Math.PI * Math.Exp((-(V + 2) / 2) * Math.Log(2, Math.E));
         Qu = U * Math.PI / 8;

         tmp1 = Math.Exp(-(Kv * Kv * (x * x + y * y) / (2 * Sigma)));
         tmp2 = Math.Cos(Kv * Math.Cos(Qu) * x + Kv * Math.Sin(Qu) * y) - Math.Exp(-(Sigma / 2));

         return (float)(tmp1 * tmp2 * Kv * Kv / Sigma);
      }

      private float KernelImgPart(int x, int y, int Orientation, int Frequency)
      {
         double U, V;
         double Sigma, Kv, Qu;
         double tmp1, tmp2;

         U = Orientation;
         V = Frequency;
         Sigma = 2 * Math.PI * Math.PI;
         Kv = Math.PI * Math.Exp((-(V + 2) / 2) * Math.Log(2, Math.E));
         Qu = U * Math.PI / 8;

         tmp1 = Math.Exp(-(Kv * Kv * (x * x + y * y) / (2 * Sigma)));
         tmp2 = Math.Sin(Kv * Math.Cos(Qu) * x + Kv * Math.Sin(Qu) * y) - Math.Exp(-(Sigma / 2));

         return (float)(tmp1 * tmp2 * Kv * Kv / Sigma);
      }

      public static Dictionary<string, GaborKernel> GaborKernelCache = new Dictionary<string, GaborKernel>();

      public static GaborKernel GetGaborKernel(short Frequency, short Size, short Orientation)
      {
         string Key = Frequency.ToString() + "_" + Size + "_" + Orientation;

         if (GaborKernelCache.ContainsKey(Key))
            return GaborKernelCache[Key];

         GaborKernel K = new GaborKernel();
         K.CalculateKernel(Orientation, Frequency, Size, Size);

         GaborKernelCache.Add(Key, K);

         return K;
      }

      public static GaborKernel GetGaborKernel(short Frequency, short SizeX, short SizeY, short Orientation)
      {
         string Key = Frequency.ToString() + "_" + SizeX + "_" + SizeY + "_" + Orientation;

         if (GaborKernelCache.ContainsKey(Key))
            return GaborKernelCache[Key];

         GaborKernel K = new GaborKernel();
         K.CalculateKernel(Orientation, Frequency, SizeX, SizeY);

         GaborKernelCache.Add(Key, K);

         return K;
      }
   }

   public class GaborEmguImg
   {
      public static Img GaborTransform(Img image, short Frequency, short Size, short Orientation)
      {
         GaborKernel Kernel = GaborKernel.GetGaborKernel(Frequency, Size, Orientation);

         Img gabor_real = image.Convolution(Kernel.KernelRealData);
         Img gabor_img = image.Convolution(Kernel.KernelImgData);

         gabor_real = gabor_real.Pow(2);
         gabor_img = gabor_img.Pow(2);

         Img gabor = (gabor_real + gabor_img);
         CvInvoke.cvSqrt(gabor.Ptr, gabor.Ptr);
         return gabor;
      }

      public static Img GaborTransform(Img image, short Frequency, short SizeX, short SizeY, short Orientation)
      {
         GaborKernel Kernel = GaborKernel.GetGaborKernel(Frequency, SizeX, SizeY, Orientation);

         Img gabor_real = image.Convolution(Kernel.KernelRealData);
         Img gabor_img = image.Convolution(Kernel.KernelImgData);

         gabor_real = gabor_real.Pow(2);
         gabor_img = gabor_img.Pow(2);

         Img gabor = (gabor_real + gabor_img);
         CvInvoke.cvSqrt(gabor.Ptr, gabor.Ptr);
         return gabor;
      }
   }
}