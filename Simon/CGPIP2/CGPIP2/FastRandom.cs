using System;
using System.Diagnostics;

namespace CGPIP2
{
   /// <summary>
   /// A fast random number generator for .NET
   /// Colin Green, January 2005
   ///
   /// September 4th 2005
   ///	 Added NextBytesUnsafe() - commented out by default.
   ///	 Fixed bug in Reinitialise() - y,z and w variables were not being reset.
   ///
   /// Key points:
   ///  1) Based on a simple and fast xor-shift pseudo random number generator (RNG) specified in:
   ///  Marsaglia, George. (2003). Xorshift RNGs.
   ///  http://www.jstatsoft.org/v08/i14/xorshift.pdf
   ///
   ///  This particular implementation of xorshift has a period of 2^128-1. See the above paper to see
   ///  how this can be easily extened if you need a longer period. At the time of writing I could find no
   ///  information on the period of System.Random for comparison.
   ///
   ///  2) Faster than System.Random. Up to 15x faster, depending on which methods are called.
   ///
   ///  3) Direct replacement for System.Random. This class implements all of the methods that System.Random
   ///  does plus some additional methods. The like named methods are functionally equivalent.
   ///
   ///  4) Allows fast re-initialisation with a seed, unlike System.Random which accepts a seed at construction
   ///  time which then executes a relatively expensive initialisation routine. This provides a vast speed improvement
   ///  if you need to reset the pseudo-random number sequence many times, e.g. if you want to re-generate the same
   ///  sequence many times. An alternative might be to cache random numbers in an array, but that approach is limited
   ///  by memory capacity and the fact that you may also want a large number of different sequences cached. Each sequence
   ///  can each be represented by a single seed value (int) when using FastRandom.
   ///
   ///  Notes.
   ///  A further performance improvement can be obtained by declaring local variables as static, thus avoiding
   ///  re-allocation of variables on each call. However care should be taken if multiple instances of
   ///  FastRandom are in use or if being used in a multi-threaded environment.
   ///
   /// </summary>
   public class FastRandom : System.Random
   {
      public static uint RandomSeed = (uint)Process.GetCurrentProcess().Id;
      public static FastRandom Rng = new FastRandom(RandomSeed);

      public static void TestRng()
      {
         /*for (int i = 0; i < 10; i++)
         {
             Console.WriteLine("RNG\t" + i + "\t" + Rng.NextDouble() + "\t" + Rng.NextBool() + "\t" + Rng.NextUInt());
         }
         */
         Reporting.Say("Random seed\t=\t=" + RandomSeed);
      }

      /* Period parameters */
      private const int N = 624;
      private const int M = 397;
      private const uint MATRIX_A = 0x9908b0df; /* constant vector a */
      private const uint UPPER_MASK = 0x80000000; /* most significant w-r bits */
      private const uint LOWER_MASK = 0x7fffffff; /* least significant r bits */

      /* Tempering parameters */
      private const uint TEMPERING_MASK_B = 0x9d2c5680;
      private const uint TEMPERING_MASK_C = 0xefc60000;

      private uint TEMPERING_SHIFT_U(uint y) { return (y >> 11); }

      private uint TEMPERING_SHIFT_S(uint y) { return (y << 7); }

      private uint TEMPERING_SHIFT_T(uint y) { return (y << 15); }

      private uint TEMPERING_SHIFT_L(uint y) { return (y >> 18); }

      private uint[] mt = new uint[N]; /* the array for the state vector  */

      private short mti;

      private uint[] mag01 = { 0x0, MATRIX_A };

      /* initializing the array with a NONZERO seed */

      public FastRandom(uint seed)
      {
         /* setting initial seeds to mt[N] using         */
         /* the generator Line 25 of Table 1 in          */
         /* [KNUTH 1981, The Art of Computer Programming */
         /*    Vol. 2 (2nd Ed.), pp102]                  */
         mt[0] = seed & 0xffffffffU;
         for (mti = 1; mti < N; ++mti)
         {
            mt[mti] = (69069 * mt[mti - 1]) & 0xffffffffU;
         }
      }

      public FastRandom()
         : this(43457) /* a default initial seed is used   */
      {
      }

      protected uint GenerateUInt()
      {
         uint y;

         /* mag01[x] = x * MATRIX_A  for x=0,1 */
         if (mti >= N) /* generate N words at one time */
         {
            short kk = 0;

            for (; kk < N - M; ++kk)
            {
               y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
               mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1];
            }

            for (; kk < N - 1; ++kk)
            {
               y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
               mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1];
            }

            y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
            mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1];

            mti = 0;
         }

         y = mt[mti++];
         y ^= TEMPERING_SHIFT_U(y);
         y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
         y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
         y ^= TEMPERING_SHIFT_L(y);

         return y;
      }

      public virtual uint NextUInt()
      {
         return this.GenerateUInt();
      }

      public virtual uint NextUInt(uint maxValue)
      {
         return (uint)(this.GenerateUInt() / ((double)uint.MaxValue / maxValue));
      }

      public virtual uint NextUInt(uint minValue, uint maxValue) /* throws ArgumentOutOfRangeException */
      {
         if (minValue >= maxValue)
         {
            throw new ArgumentOutOfRangeException();
         }

         return (uint)(this.GenerateUInt() / ((double)uint.MaxValue / (maxValue - minValue)) + minValue);
      }

      public override int Next()
      {
         return this.Next(int.MaxValue);
      }

      public override int Next(int maxValue) /* throws ArgumentOutOfRangeException */
      {
         if (maxValue <= 1)
         {
            if (maxValue < 0)
            {
               throw new ArgumentOutOfRangeException();
            }

            return 0;
         }

         return (int)(this.NextDouble() * maxValue);
      }

      public new int Next(int minValue, int maxValue)
      {
         if (maxValue < minValue)
         {
            throw new ArgumentOutOfRangeException();
         }
         else if (maxValue == minValue)
         {
            return minValue;
         }
         else
         {
            return this.Next(maxValue - minValue) + minValue;
         }
      }

      public override void NextBytes(byte[] buffer) /* throws ArgumentNullException*/
      {
         int bufLen = buffer.Length;

         if (buffer == null)
         {
            throw new ArgumentNullException();
         }

         for (int idx = 0; idx < bufLen; ++idx)
         {
            buffer[idx] = (byte)this.Next(256);
         }
      }

      public double NextNormal(double mean, double stdDev)
      {
         double u1 = this.NextDouble(); //these are uniform(0,1) random doubles
         double u2 = this.NextDouble();
         double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                      Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
         double randNormal =
                      mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
         return randNormal;
      }

      public override double NextDouble()
      {
         return (double)this.GenerateUInt() / ((ulong)uint.MaxValue + 1);
      }

      public bool NextBool()
      {
         return NextDouble() < 0.5d;
      }
   }
}