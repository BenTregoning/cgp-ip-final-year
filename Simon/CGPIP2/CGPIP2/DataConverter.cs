﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;

namespace CGPIP2
{
    public class DataConverter
    {
        
        public static void Innervision()
        {
            string[] InputFiles = Directory.GetFiles("C:\\work\\innvervision\\oct2012\\3d1\\", "*.jpg");
            string[] TargetFiles = Directory.GetFiles("C:\\work\\innvervision\\oct2012\\lebeled-4939_SiteProstate_SectionLong_TransSIUIU5L50E_TGCNonVPSPatient321-3d-001\\", "*.jpg");
            string[] SGYFiles = Directory.GetFiles("Z:\\snapshots\\work\\innvervision\\Patient0321_Record003105106_SiteProstate_SectionLong_TransSIUIU5L50E_TGCNonVPSPatient312-001_Raw", "*.sgy");
            string SGYOutputFolder = "Z:\\snapshots\\work\\innvervision\\Patient0321_Subset\\";
            if (!Directory.Exists(SGYOutputFolder))
                Directory.CreateDirectory(SGYOutputFolder);
            foreach (string TargetFile in TargetFiles)
            {
                string TargetFileNameOnly = Path.GetFileNameWithoutExtension(TargetFile);
                string ID = TargetFileNameOnly.Substring(TargetFileNameOnly.IndexOf("-") + 1, 4);
                string SGYFile = "";

                foreach (string C in SGYFiles)
                {
                    if (C.Contains(ID))
                        SGYFile = C;
                }

                Console.WriteLine(Path.GetFileNameWithoutExtension(TargetFile));
                Console.WriteLine(Path.GetFileNameWithoutExtension(SGYFile));
                if (SGYFile.Contains("_Raw.sgy"))
                {
                    File.Copy(SGYFile, SGYOutputFolder + "//" + Path.GetFileName(SGYFile));
                    Directory.CreateDirectory(SGYOutputFolder + "//" + ID);
                }
            }

            return;
            string DestInputs = "C:\\work\\innvervision\\oct2012\\Training\\Inputs\\";
            string DestTargets = "C:\\work\\innvervision\\oct2012\\Training\\Targets\\";
            if (!Directory.Exists(DestInputs))
                Directory.CreateDirectory(DestInputs);
            if (!Directory.Exists(DestTargets))
                Directory.CreateDirectory(DestTargets);

            int Counter = 0;
            foreach (string TargetFile in TargetFiles)
            {
                //labeled-4926_SiteProstate_SectionLong_TransSIUIU5L50E_TGCNonVPSPatient321-3d-001.jpg
                string TargetFileNameOnly = Path.GetFileNameWithoutExtension(TargetFile);
                string ID = TargetFileNameOnly.Substring(TargetFileNameOnly.IndexOf("-")+1, 4);

                string InputFile = "";

                foreach (string C in InputFiles)
                {
                    if (C.Contains(ID))
                        InputFile = C;
                }

                if (File.Exists(InputFile))
                {
                    Reporting.Say(InputFile);


                    Image<Bgr, float> A = new Image<Bgr, float>(InputFile);
                    A = A.Resize(512, 512, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    Image<Bgr, float> B = new Image<Bgr, float>(TargetFile);
                    double[] angles = { -10, 0, 10};

                    foreach(double r in angles)
                    {
                        Image<Bgr, float> a = A.Clone(); ;
                        Image<Bgr, float> b = B.Clone();
                        if (r <-1 || r>1)
                        {
                        //    A = A.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
                          //  B = B.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
                            a = A.Rotate(r, new Bgr(0, 0, 0), true);
                           b = B.Rotate(r, new Bgr(255, 0, 0), true);
                        }
                        
                        a.Save(DestInputs + "\\" + Counter + "_" + r + ".png");
                        b.Save(DestTargets + "\\" + Counter + "_" + r + ".png");
                    }

                    Counter++;
                }
                Reporting.Say(ID);
                
            }
        }
    }
}
