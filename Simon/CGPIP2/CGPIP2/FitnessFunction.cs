﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace CGPIP2
{
    public class FitnessFunctionResult
    {
        public ConfusionMatrix[] StatsPerTestCase;
        public double[] DifferencePerTestCase = null;

        public double Error = double.MaxValue;

        public double Duration;
        public int TotalEvaluatedNodeCount;


        public void ComputeFinalStats(int TestCaseCount)
        {
            if (Parameters.FitnessAggregationType == Parameters.FitnessAggregation.ALL)
                ComputeFinalStats_all(TestCaseCount);
            else if (Parameters.FitnessAggregationType == Parameters.FitnessAggregation.AVERAGE || Parameters.FitnessAggregationType == Parameters.FitnessAggregation.WORST)
                ComputeFinalStats_avg(TestCaseCount);

        }

        public void ComputeFinalStats_all(int TestCaseCount)
        {
            int Count = 0;

            if (Parameters.FitnessScoreType == Parameters.FitnessType.Difference)
            {
                this.Error = 0;

                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc])
                    {
                        this.Error += this.DifferencePerTestCase[tc];
                        Count++;
                    }
                }


                this.Error /= Count;
                if (double.IsNaN(this.Error) || double.IsInfinity(this.Error))
                    this.Error = double.MaxValue;



            }

            if (Parameters.FitnessScoreType == Parameters.FitnessType.Accuracy)
            {
                this.Error = 0;


                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc])
                    {
                        this.Error += StatsPerTestCase[tc].Accuracy;
                        Count++;
                    }
                }
                this.Error /= Count;
                if (double.IsNaN(this.Error) || double.IsInfinity(this.Error))
                    this.Error = double.MaxValue;

            }

            if (Parameters.FitnessScoreType == Parameters.FitnessType.MCC)
            {
                this.Error = 0;

                ConfusionMatrix CM = new ConfusionMatrix();


                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                    if (Parameters.EvaluationMask[tc] && StatsPerTestCase[tc] != null)
                        CM.Add(StatsPerTestCase[tc]);

                this.Error = 1d - Math.Abs(CM.MCC);
                if (this.Error < -1 || this.Error > 1 || double.IsInfinity(this.Error) || double.IsNaN(this.Error))
                    throw new Exception("MCC out of range");
            }
        }

        public void ComputeFinalStats_avg(int TestCaseCount)
        {

            if (Parameters.FitnessScoreType == Parameters.FitnessType.MCC)
            {
                this.Error = 0;

                double WorstFitness = 0;

                double[] MCCs = new double[Parameters.EvaluationMask.Length];


                int CountMCCs = 0;
                int CountPositiveMCC = 0;
                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc] && StatsPerTestCase[tc] != null)
                    {
                        MCCs[tc] = StatsPerTestCase[tc].MCCFast;
                        CountMCCs++;
                        if (MCCs[tc] >= 0)
                            CountPositiveMCC++;
                    }
                }

                bool TreatAsPositiveMCC = CountPositiveMCC >= (CountMCCs / 2);
                double Sum = 0;
                int Count = 0;


                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc] && StatsPerTestCase[tc] != null)
                    {
                        double MCC = MCCs[tc];
                        if (TreatAsPositiveMCC)
                        {
                            if (MCC < 0) MCCs[tc] = 1;
                            else MCCs[tc] = 1d - MCC;
                        }
                        else
                        {
                            if (MCC >= 0) MCCs[tc] = 1;
                            else MCCs[tc] = 1d - Math.Abs(MCC);
                        }

                        Sum += MCCs[tc];
                        Count++;
                        if (MCCs[tc] > WorstFitness)
                            WorstFitness = MCCs[tc];
                    }
                }

                this.Error = Sum / Count; //if going for avg
                /*
                double StdDev = 0;
                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc] && StatsPerTestCase[tc] != null)
                    {                        
                        StdDev += Math.Pow(MCCs[tc] - this.Error, 2);                        
                    }
                }
                StdDev = Math.Sqrt(StdDev);

                double SumOutlier = 0;
                int CountOutlier = 0;
                for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
                {
                    if (Parameters.EvaluationMask[tc] && StatsPerTestCase[tc] != null)
                    {                        
                        if (MCCs[tc] > this.Error + (StdDev/2))
                        {
                            SumOutlier += MCCs[tc];
                            CountOutlier++;
                        }
                    }
                }
                */
                /*if (CountOutlier > 1)
                {
                    Reporting.Say("Outliers detected " + CountOutlier);
                   // this.Error = SumOutlier / CountOutlier;
                }*/

                if (Parameters.FitnessAggregationType == Parameters.FitnessAggregation.WORST)
                    this.Error = WorstFitness;

            }
            else
                throw new Exception("Non MCC fitness is not supported");
        }


        public FitnessFunctionResult Clone()
        {
            FitnessFunctionResult R = new FitnessFunctionResult();
            R.Error = this.Error;
            R.Duration = this.Duration;
            R.TotalEvaluatedNodeCount = this.TotalEvaluatedNodeCount;
            if (this.DifferencePerTestCase != null)
            {
                R.DifferencePerTestCase = new double[this.DifferencePerTestCase.Length];
                Array.Copy(this.DifferencePerTestCase, R.DifferencePerTestCase, this.DifferencePerTestCase.Length);
            }
            if (StatsPerTestCase != null)
            {
                R.StatsPerTestCase = new ConfusionMatrix[this.StatsPerTestCase.Length];
                for (int i = 0; i < this.StatsPerTestCase.Length; i++)
                {
                    R.StatsPerTestCase[i] = this.StatsPerTestCase[i] == null ? null : this.StatsPerTestCase[i].Clone();
                }
            }
            return R;
        }
    }

    public class FitnessFunction
    {
        public static CGPImageTestSet TestSet = null;
        public static ulong EvaluationCount = 0;
        public static ulong ImageEvaluationCount = 0;

        public double[] DoublesToArray(params double[] Values)
        {
            return Values;
        }


        public FitnessFunction()
        {
            if (TestSet != null) return;

            if (Parameters.LoadFromPrecooked)
            {
                TestSet = new CGPImageTestSet();
                TestSet.LoadFromPrecooked();
            }
            else
            {
                TestSet = new CGPImageTestSet(
                    Parameters.TestCasesInputFolder,
                    Parameters.TestCasesTargetsFolder,
                    Parameters.ImageFilter,
                    Parameters.ScaleInputs,
                    Parameters.SplitHSB,
                    Parameters.SplitBGR,
                    Parameters.FilterTargetsForRed,
                    Parameters.FilterTargetsForBlue,
                    Parameters.InvertTargetClasses);
            }
            int MaxDimension = 0;
            foreach (CGPImageTestCase TC in TestSet.TestCases)
                MaxDimension = Math.Max(MaxDimension, Math.Max(TC.Target.Width, TC.Target.Height));
            if (Parameters.VisualizationSize > MaxDimension)
                Parameters.VisualizationSize = MaxDimension;

            if (Parameters.ValidationSplit > 0)
                TestSet.LabelTestAndTraining(Parameters.ValidationSplit);

            TestSet.MakeCheckSums();

            Parameters.EvaluationMask = new bool[TestSet.TestCases.Count];
            for (int i = 0; i < TestSet.TestCases.Count; i++)
                Parameters.EvaluationMask[i] = true;

            Parameters.InputCount = TestSet.TestCases[0].Inputs.Count;
            Parameters.MaxFilesToLoad = TestSet.TestCases.Count;
            Parameters.MaxFilesToToTest = TestSet.TestCases.Count;
            Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;

            if (Parameters.SaveToPrecooked)
            {
                Reporting.Say("Writing inputs to precooked file " + Parameters.SaveToPrecooked);
                TestSet.SaveToPrecooked();
                Reporting.Say("Writtnen inputs to precooked file " + Parameters.SaveToPrecooked);
                Application.Exit();
            }
        }

        public void obsoleteFitnessFunction()
        {
            if (TestSet == null)
            {
                /*    TestSet = new CGPImageTestSet(
                    "C:/work/Penguins/Inputs/",
                    "C:/work/Penguins/Targets/",
                    "*.jpg",
                    Parameters.ScaleInputs,
                    true,
                    true,
                    true);
                     */
                /*Parameters.MaxFilesToLoad = 150;
                Parameters.MaxImageDimension = 1024;
                Parameters.Rescales = DoublesToArray( 0.25, 0.1);
                Parameters.VisualizationSize = 256;
                Parameters.ValidationSplit = 0.5;
                TestSet = new CGPImageTestSet(
                            "C:\\work\\icubFingers2\\Inputs\\",
                            "C:\\work\\icubFingers2\\Targets\\",
                            "*.png",
                            true,
                            true,
                            true,
                            false,
                            false, false);*/

                /* Parameters.MaxFilesToLoad = 150;
                 Parameters.MaxImageDimension = 1024;
                 Parameters.Rescales = DoublesToArray(1, 0.5, 0.25, 0.1);
                 Parameters.VisualizationSize = 256;
                 Parameters.ValidationSplit = 0.5;
                 TestSet = new CGPImageTestSet(
                             "C:\\work\\csharp\\CGPIP2DataSets\\MarsFlower\\Inputs\\",
                             "C:\\work\\csharp\\CGPIP2DataSets\\MarsFlower\\Targets\\",
                             "*.jpg",
                             true,
                             true,
                             true,
                             true,
                             false, false);*/
                /* Parameters.MaxFilesToLoad = 34;
                   Parameters.MaxImageDimension = 640;
                   Parameters.Rescales = DoublesToArray(0.25);
                   Parameters.VisualizationSize = 256;
                   Parameters.ValidationSplit = 0.5;
                   Parameters.FitnessAggregationType = Parameters.FitnessAggregation.ALL;
                   Parameters.ImageBorder = 5;
                   TestSet = new CGPImageTestSet(
                               "C:\\work\\csharp\\CGPIP2DataSets\\TestSetJan2013Fingertips\\input\\",
                               "C:\\work\\csharp\\CGPIP2DataSets\\TestSetJan2013Fingertips\\target\\",
                               //"C:\\work\\csharp\\CGPIP2DataSets\\TestSetJan2013Fingertips\\input\\",
                               //"C:\\work\\csharp\\CGPIP2DataSets\\TestSetJan2013Fingertips\\target\\",
                               //"C:\\work\\csharp\\CGPIP2DataSets\\ValidationSetJan2013\\input\\",
                               //"C:\\work\\csharp\\CGPIP2DataSets\\ValidationSetJan2013\\targets_fingertips\\",
                               "*.png",
                               true,
                               true,
                               true,
                               true,
                               false, false);
                   Parameters.ValidationSplit = 0.3;
                   Parameters.VisualizationSize = 256;
                   Parameters.MedianFilterOutput = 0;
                   TestSet.LabelTestAndTraining(Parameters.ValidationSplit);*/
                /*Parameters.MaxFilesToLoad = 44;
                Parameters.MaxImageDimension = 640;
                Parameters.Rescales = null;// DoublesToArray(1);
                Parameters.RescaleToFit = new System.Drawing.Size[] 
                {
                    new Size(128,-1)
                };
                Parameters.VisualizationSize = 256;
                Parameters.ValidationSplit = 0.5;
                Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                Parameters.ImageBorder = 5;

                if (Directory.Exists("C:\\work\\Bristol\\SpotsDetectorTraining\\Inputs\\"))
                {
                    TestSet = new CGPImageTestSet(
                                "C:\\work\\Bristol\\SpotsDetectorTraining\\Inputs\\",
                                "C:\\work\\Bristol\\SpotsDetectorTraining\\Targets\\",
                                "*.png",
                                true,
                                true,
                                true,
                                true,
                                true, false);
                }
                else
                {
                    TestSet = new CGPImageTestSet(
                                "C:\\Users\\Simon\\Project\\CGPForSpotDetection\\SpotsDetectorTraining\\Inputs\\",
                                "C:\\Users\\Simon\\Project\\CGPForSpotDetection\\SpotsDetectorTraining\\Targets\\",
                                "*.png",
                                true,
                                true,
                                true,
                                true,
                                true, false);
                }
                Parameters.ValidationSplit = 0.3;
                Parameters.VisualizationSize = 256;
                Parameters.MedianFilterOutput = 0;*/
                //    TestSet.LabelTestAndTraining(Parameters.ValidationSplit);


                /*  Parameters.Rescales = DoublesToArray( 0.2);
                  Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                  Parameters.MaxFilesToToTest = Parameters.Replay ? 500 : 8;// 128;
                  Parameters.MaxFilesToLoad = 1000;
                  Parameters.MaxImageDimension = 128;
                  Parameters.VisualizationSize = 64;
                  Parameters.ImageBorder = 2;
                  if (Parameters.Replay)
                      TestSet = new CGPImageTestSet("C:\\work\\Bristol\\Chimps\\cgp\\unseen_withsrcimg\\   ", true, false, false);
                  else
                      TestSet = new CGPImageTestSet("C:\\work\\Bristol\\Chimps\\cgp\\unseen_withsrcimg_subset\\   ", true, false, false);

                  */
                /*            TestSet = new CGPImageTestSet(
                          "C:/work/Bristol/TrainingImages/",
                           Parameters.ScaleInputs, true);*/
                /*TestSet = new CGPImageTestSet(
               "C:\\work\\chars\\Inputs\\",
               "C:\\work\\chars\\Targets\\",
               "*.png",
               Parameters.ScaleInputs,
               true,
               true,
               true);*/
                /*Parameters.ScaleInputs = 100;
                Parameters.MaxImageDimension = 96;
                Parameters.VisualizationSize = 128;
                Parameters.MaxFilesToLoad = 150;
                TestSet = new CGPImageTestSet("C:\\work\\Bristol\\TrainingImagesForCGP\\Processed\\",Parameters.ScaleInputs, true);
              */

                /*Parameters.Rescales = DoublesToArray(0.1, 0.15, 0.25);
                 Parameters.FitnssAggregationType = Parameters.FitnessAggregation.AVERAGE;
                 Parameters.MaxFilesToToTest = 10;
                 Parameters.MaxFilesToLoad = 121;
                 Parameters.MaxImageDimension = 256;
                 Parameters.VisualizationSize = 128;
                 Parameters.ImageBorder = 2;
                 if (Directory.Exists("C:\\Users\\Simon\\Project\\cgp\\Processed5_bodyonlycascades\\"))
                     TestSet = new CGPImageTestSet("C:\\Users\\Simon\\Project\\cgp\\Processed5_bodyonlycascades\\", true, true, false);
                 else
                     TestSet = new CGPImageTestSet("C:\\work\\Bristol\\TrainingImagesForCGP\\Processed5_bodyonlycascades\\", true, true, false);
             */


                /*   Parameters.Rescales = DoublesToArray( 0.25);
                     Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                     Parameters.MaxFilesToToTest =3;
                     Parameters.MaxFilesToLoad = 256;
                     Parameters.MaxImageDimension = 512;
                     Parameters.IncreaseThreshold = 0.5;
                     Parameters.ImageBorder = 1;
                     Parameters.PreComputeAdditionalInputs = true;
                     TestSet = new CGPImageTestSet(
                  //  "C:\\work\\RealSteel\\binROI\\train\\cra\\input\\",
                   // "C:\\work\\RealSteel\\binROI\\train\\cra\\target\\",
                   "C:\\work\\RealSteel\\binROI\\randSampled_1000\\inputs\\",
                   "C:\\work\\RealSteel\\binROI\\randSampled_1000\\targets\\",
                    "*.png",
                    true,
                    false,
                    false,
                    false,
                    false,
                    false);
                     TestSet.LabelTestAndTraining(Parameters.ValidationSplit);
                     Parameters.VisualizationSize = 64;
               */
                Parameters.Rescales = DoublesToArray(0.25);
                Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                Parameters.MaxFilesToToTest = 1;
                Parameters.MaxFilesToLoad = 256;
                Parameters.MaxImageDimension = 1024;
                Parameters.IncreaseThreshold = 0.5;
                Parameters.ImageBorder = 2;
                Parameters.PreComputeAdditionalInputs = true;
                TestSet = new CGPImageTestSet(
                    //  "C:\\work\\RealSteel\\binROI\\train\\cra\\input\\",
                    // "C:\\work\\RealSteel\\binROI\\train\\cra\\target\\",
              Parameters.TestCasesInputFolder,
              Parameters.TestCasesTargetsFolder,
              Parameters.ImageFilter,
               Parameters.ScaleInputs,
               Parameters.SplitHSB,
               Parameters.SplitBGR,
               Parameters.FilterTargetsForRed,
               Parameters.FilterTargetsForBlue,
               Parameters.InvertTargetClasses);


                if (Parameters.ValidationSplit > 0)
                    TestSet.LabelTestAndTraining(Parameters.ValidationSplit);
                Parameters.VisualizationSize = 512;


                //TestSet = Tests.MakeScantr               onTestset();



                // TestSet.LabelTestAndTraining(Parameters.ValidationSplit);
                /*Parameters.Rescales = DoublesToArray(0.25);
                 Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                 Parameters.MaxFilesToToTest = 9;
                 Parameters.MaxFilesToLoad = 9;
                 Parameters.MaxImageDimension = 256;
                 Parameters.IncreaseThreshold = 0.5;
                 Parameters.MaxGenotypes = 5;
                 Parameters.VisualizationSize = 256;
                
                       TestSet = new CGPImageTestSet("C:\\work\\innvervision\\oct2012\\", true, true, false);*/
                //      TestSet.TestCases.Reverse();


                /* Parameters.Rescales = DoublesToArray(0.5);
                 Parameters.FitnessAggregationType = Parameters.FitnessAggregation.AVERAGE;
                 Parameters.MaxFilesToToTest = 3;
                 Parameters.MaxFilesToLoad = 49;
                 Parameters.MaxImageDimension = 128;
                
                 Parameters.ImageBorder = 4;
                 Parameters.PreComputeAdditionalInputs = false;
                 TestSet = new CGPImageTestSet(
                "C:\\work\\neurons2\\crops2\\Inputs\\",
                "C:\\work\\neurons2\\crops2\\Targets\\",
                "*.png",
                true,
                false,
                false,
                false,
                false,true);
                 Parameters.VisualizationSize = TestSet.TestCases[0].Inputs[0].Width;
                 Parameters.ValidationSplit = 0.3;*/
                // TestSet.LabelTestAndTraining(Parameters.ValidationSplit);

                /*  Parameters.Rescales = DoublesToArray(0.25,0.5, 0.1);
                  Parameters.MaxFilesToToTest = 3;
                  Parameters.MaxFilesToLoad = 22;
                  Parameters.MaxImageDimension = 256;
                  Parameters.VisualizationSize = 128;
                  TestSet = new CGPImageTestSet(
                 "C:\\work\\mias\\InputsCALC4\\",
                 "C:\\work\\mias\\TargetsCALC4\\",
                 "*.png",            
                 true,
                 false,
                 false,
                 true, true,false);
                  Parameters.FitnssAggregationType = Parameters.FitnessAggregation.AVERAGE;
                  */
                /*Parameters.Rescales = DoublesToArray(0.25, 0.5, 0.1);
                Parameters.MaxFilesToToTest = 3;
                Parameters.MaxFilesToLoad = 22;
                Parameters.MaxImageDimension = 256;
                Parameters.VisualizationSize = 128;
                TestSet = new CGPImageTestSet(
               "C:\\work\\NaoFingers\\Inputs\\",
               "C:\\work\\NaoFingers\\Targets\\",
               "*.png",
               true,
               true,
               true,
               true, false, false);
                Parameters.FitnssAggregationType = Parameters.FitnessAggregation.AVERAGE;*/
                /*
                Parameters.MaxFilesToLoad = 150;
                Parameters.MaxImageDimension = 256;
                Parameters.Rescales = DoublesToArray(0.5, 0.1, 0.25);
                TestSet = new CGPImageTestSet(
                            "C:\\work\\Bristol\\TrainingImagesForCGP\\spots\\inputs\\",
                            "C:\\work\\Bristol\\TrainingImagesForCGP\\spots\\Targets\\",
                            "*.jpg",
                            true,
                            true,
                            true,
                            true,
                            false, false);
                //TestSet.TestCases[1].UseType = CGPImageTestCase.TestCaseUseType.Validation;
                Parameters.MinImageDimension = 16;
                Parameters.ImageBorder = 4;
                Parameters.VisualizationSize = 128;*/


                //TestSet.LabelTestAndTraining(Parameters.ValidationSplit);

                /*  Parameters.MaxFilesToLoad = 150;
                  Parameters.MaxImageDimension = 1024;
                  Parameters.Rescales = DoublesToArray(1,0.5,0.1, 0.25);
                  TestSet = new CGPImageTestSet(
                              "C:\\work\\marsoutcrop\\Inputs2\\",
                              "C:\\work\\marsoutcrop\\Targets2\\",
                              "*.png",
                              true,
                              true,
                              true,
                              true,
                              false, false);
                  //TestSet.TestCases[1].UseType = CGPImageTestCase.TestCaseUseType.Validation;
                  Parameters.VisualizationSize = 512;*/

                TestSet.MakeCheckSums();

                //TestSet.LabelTestAndTraining(Parameters.ValidationSplit);

                Parameters.EvaluationMask = new bool[TestSet.TestCases.Count];
                for (int i = 0; i < TestSet.TestCases.Count; i++)
                    Parameters.EvaluationMask[i] = true;

                Parameters.InputCount = TestSet.TestCases[0].Inputs.Count;


                Parameters.MaxFilesToLoad = TestSet.TestCases.Count;
                Parameters.InputCount = TestSet.TestCases[0].Inputs.Count;
            }
        }


  
        public FitnessFunctionResult Test(CGPIndividual Ind, PopulationParameters PopParams)
        {
            EvaluationCount++;
            Ind.EvaluationIndex = EvaluationCount;
            Ind.Evaluated = true;
            FitnessFunctionResult Result = new FitnessFunctionResult();
            Ind.Dirty = false;
            if (Parameters.FitnessScoreType == Parameters.FitnessType.Difference)
            {
                Result.DifferencePerTestCase = new double[TestSet.TestCases.Count];

            }
            else if (Parameters.FitnessScoreType == Parameters.FitnessType.Accuracy || Parameters.FitnessScoreType == Parameters.FitnessType.MCC)
            {
                Result.StatsPerTestCase = new ConfusionMatrix[TestSet.TestCases.Count];
            }


            Result.TotalEvaluatedNodeCount = 0;

            CGPGraphRunner[] Runners = new CGPGraphRunner[Ind.Genotypes.Count];
            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                int EvaluatedNodeCount;
                Runners[g] = new CGPGraphRunner();
                Runners[g].FindNodesToProcess(Ind.Genotypes[g], Parameters.OutputCount, out EvaluatedNodeCount);

                Result.TotalEvaluatedNodeCount += EvaluatedNodeCount;
            }

            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                Ind.Thresholds[g] = 128;
            }


            Result.Duration = 0;


            if (Result.TotalEvaluatedNodeCount == 0)
            {
                Result.Error = double.MaxValue;
                return Result;
            }


            for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
            {
                TextWriter GraphOutput = null;

                if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                {
                    if (!Directory.Exists(Parameters.TraceOutputFolder))
                        Directory.CreateDirectory(Parameters.TraceOutputFolder);
                    GraphOutput = new StreamWriter(String.Format("{1}/graph_{0:000000000}.gv", EvaluationCount, Parameters.TraceOutputFolder));
                    GraphOutput.WriteLine("Graph G { ");
                }
                if (Parameters.ShowTrace && tc == 0)
                {
                    for (int g = 0; g < Ind.Genotypes.Count; g++)
                    {
                        if (g == 0)
                            Ind.MixModes[g] = 4;
                        Reporting.Say("THRESHOLD\t" + Ind.Thresholds[g]);
                        switch (Ind.MixModes[g])
                        {
                            case (0):
                                Reporting.Say("MIXMODE\t" + g + "\tOR");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"OR\",image=\"" + String.Format("{1}/output_t_{0:00}.png", g, Parameters.TraceOutputFolder) + "\",labelloc=b];");
                                }
                                break;
                            case (1):
                                Reporting.Say("MIXMODE\t" + g + "\tAND");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"AND\",image=\"" + String.Format("{1}/output_t_{0:00}.png", g, Parameters.TraceOutputFolder) + "\",labelloc=b];");
                                }
                                break;
                            case (2):
                                Reporting.Say("MIXMODE\t" + g + "\tXOR");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"XOR\",image=\"" + String.Format("{1}/output_t_{0:00}.png", g, Parameters.TraceOutputFolder) + "\",labelloc=b];");
                                }
                                break;
                            case (3):
                                Reporting.Say("MIXMODE\t" + g + "\tNAND");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"NAND\"];");
                                }
                                break;
                            case (4):
                                Reporting.Say("MIXMODE\t" + g + "\tA");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"STOP\",image=\"" + String.Format("{1}/output_t_{0:00}.png", g, Parameters.TraceOutputFolder) + "\",labelloc=b];");
                                }
                                break;
                        }
                    }
                }

                if (!Parameters.EvaluationMask[tc])
                {
                    if (Parameters.ShowTrace && Parameters.Visualize)
                    {
                        if (ImageMatrix.ImageMatrixForm.ImageCells[tc] != null)
                            ImageMatrix.ImageMatrixForm.ImageCells[tc].Dispose();
                        ImageMatrix.ImageMatrixForm.ImageCells[tc] = null;
                    }
                    continue;
                }
                CGPImageTestCase TestCase = TestSet.TestCases[tc];

                if (TestCase.UseType == CGPImageTestCase.TestCaseUseType.Validation && !Parameters.ProcessValidation)
                {
                    Result.StatsPerTestCase[tc] = null;
                    //     Console.Write(".");
                    continue;
                }

                Parameters.DefaultImageHeight = TestCase.Target.Height;
                Parameters.DefaultImageWidth = TestCase.Target.Width;
                Parameters.InputCount = TestCase.Inputs.Count;


                List<CGPImage> Outputs = new List<CGPImage>();
                List<CGPImage> ThresholdedOutputs = new List<CGPImage>();

                for (int g = 0; g < Ind.Genotypes.Count; g++)
                {
                    List<CGPImage> Inputs = new List<CGPImage>();

                    Inputs.AddRange(TestCase.Inputs);

                    if (Outputs.Count > 0)
                    {
                        Inputs.AddRange(Outputs);
                        Inputs.AddRange(ThresholdedOutputs);
                    }

                    if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                    {
                        for (int i = 0; i < Inputs.Count; i++)
                        {
                            Inputs[i].Save(String.Format("{2}/inputs_{0:00}_{1:000}.png", g, i, Parameters.TraceOutputFolder));
                        }
                    }

                    Parameters.InputCount = Inputs.Count;
                    Runners[g].Run(Ind.Genotypes[g], Inputs.ToArray(), tc, g.ToString());

                    Runners[g].CleanUp();

                    Result.Duration += Runners[g].Duration;
                    CGPImage Output = Runners[g].GetOutput();


                    if (Parameters.MedianFilterOutput > 0)
                        Output = CGPImageFunctions.SmoothMedian(Output, Parameters.MedianFilterOutput);
                    //                    if (Parameters.ShowTrace && Parameters.Visualize)
                    CGPImage ThresholdedOutput = CGPImageFunctions.Threshold(Output, Ind.Thresholds[g]);

                    if (ThresholdedOutput.Width != TestCase.Target.Width || ThresholdedOutput.Height != TestCase.Target.Height)
                        ThresholdedOutput = CGPImageFunctions.Rescale(ThresholdedOutput, TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_NN);


                    ThresholdedOutputs.Add(ThresholdedOutput);

                    if (Output.Width != TestCase.Target.Width || Output.Height != TestCase.Target.Height)
                        Output = CGPImageFunctions.Rescale(Output, TestCase.Target.Width, TestCase.Target.Height);

                    Outputs.Add(Output);

                    if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                    {
                        Output.Save(String.Format("{1}/Output_beforeThreshold{0}.png", g, Parameters.TraceOutputFolder));
                        ThresholdedOutput.Save(String.Format("{1}/Output_{0}.png", g, Parameters.TraceOutputFolder));
                    }

                    if (GraphOutput != null)
                    {
                        GraphOutput.Write(Runners[g].GraphText);
                        GraphOutput.WriteLine();
                    }
                }

                if (GraphOutput != null)
                {
                    GraphOutput.WriteLine("}");
                    GraphOutput.Close();

                    try
                    {
                        ProcessStartInfo PSI = new ProcessStartInfo("dot", "-Tpng " + String.Format("graph_{0:000000000}.gv", EvaluationCount) + " -o " + String.Format("graph_{0:000000000}.png", EvaluationCount));
                        PSI.WorkingDirectory = Parameters.TraceOutputFolder;
                        PSI.UseShellExecute = false;
                        PSI.CreateNoWindow = false;
                        Process.Start(PSI);
                    }
                    catch (Exception e)
                    {

                    }
                }


                bool[,] CombinedOutput = new bool[TestCase.Target.Height, TestCase.Target.Width];


                for (int x = 0; x < TestCase.Target.Width; x++)
                    for (int y = 0; y < TestCase.Target.Height; y++)
                        for (int g = 0; g < Ind.Genotypes.Count; g++)
                        {
                            float v = Outputs[g].Img.Data[y, x, 0];



                            if (float.IsNaN(v) || float.IsInfinity(v))
                                v = 0;

                            bool SingleOutput = v > Ind.Thresholds[g];
                            if (g == 0)
                                Ind.MixModes[g] = 4;
                            switch (Ind.MixModes[g])
                            {
                                case (0):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] | SingleOutput;
                                    break;
                                case (1):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] & SingleOutput;
                                    break;
                                case (2):
                                    CombinedOutput[y, x] = CombinedOutput[y, x] ^ SingleOutput;
                                    break;
                                case (3):
                                    CombinedOutput[y, x] = !(CombinedOutput[y, x] & SingleOutput);
                                    break;
                                case (4):
                                    CombinedOutput[y, x] = SingleOutput;
                                    break;
                            }

                        }



                if (Parameters.ShowTrace)
                {
                    Image<Gray, byte> FP = new Image<Gray, byte>(TestCase.Target.Width, TestCase.Target.Height);
                    Image<Gray, byte> FN = new Image<Gray, byte>(TestCase.Target.Width, TestCase.Target.Height);

                    for (int x = 0; x < TestCase.Target.Width; x++)
                        for (int y = 0; y < TestCase.Target.Height; y++)
                        {
                            bool Expected = TestCase.Target.Img.Data[y, x, 0] > 128;
                            bool Predicted = CombinedOutput[y, x];
                            if (Expected && !Predicted)
                                FN.Data[y, x, 0] = 255;
                            if (!Expected && Predicted)
                                FP.Data[y, x, 0] = 255;
                        }

                    if (!Directory.Exists("./fn/"))
                        Directory.CreateDirectory("./fn/");
                    if (!Directory.Exists("./fp/"))
                        Directory.CreateDirectory("./fp/");
                    FP.Save("./fp/" + Path.GetFileName(TestCase.TargetFileName));
                    FN.Save("./fn/" + Path.GetFileName(TestCase.TargetFileName));
                }

                ImageEvaluationCount++;



                if (Parameters.FitnessScoreType == Parameters.FitnessType.Accuracy || Parameters.FitnessScoreType == Parameters.FitnessType.MCC)
                    Result.StatsPerTestCase[tc] = TestCase.Compare(CombinedOutput, TestCase.Target.Width, TestCase.Target.Height, Parameters.ImageBorder, 0, 1);

                Result.StatsPerTestCase[tc].UseType = TestCase.UseType;

                if (Parameters.ImageIsClassification)
                {
                    bool Expected = TestCase.Target.Img.Data[TestCase.Target.Height / 2, TestCase.Target.Width / 2, 0] > 128;
                    /* ulong Count0 = 0;
                     ulong Count1 = 0;
                     for (int x = 0; x < TestCase.Target.Width; x++)
                         for (int y = 0; y < TestCase.Target.Height; y++)
                             if (CombinedOutput[y, x])
                                 Count1++;
                             else
                                 Count0++;

                     bool Predicted = Count1 > Count0;
                     */
                    bool Predicted = CombinedOutput[TestCase.Target.Height / 2, TestCase.Target.Width / 2];
                    Result.StatsPerTestCase[tc].ExpectedClass = Expected;
                    Result.StatsPerTestCase[tc].PredictedClass = Predicted;
                    if (Result.StatsPerTestCase[tc].MCC < 0)
                        Result.StatsPerTestCase[tc].PredictedClass = !Result.StatsPerTestCase[tc].PredictedClass;
                }

                if (Parameters.ShowTrace)
                {
                    Reporting.Say(String.Format("TESTCASERESULT\t{0,16}\t{1,4}\t{2,4},{3,4},{4,4},{5,4}\t{6,8}\t{7,8}\t{8,8}\t{9,8}\t{10,8}",
                        Path.GetFileNameWithoutExtension(TestCase.InputFileName),
                        tc,
                         Result.StatsPerTestCase[tc].TP,
                         Result.StatsPerTestCase[tc].FP,
                         Result.StatsPerTestCase[tc].TN,
                         Result.StatsPerTestCase[tc].FN,
                         Result.StatsPerTestCase[tc].Accuracy,
                         Result.StatsPerTestCase[tc].MCC,
                         (Result.StatsPerTestCase[tc].ExpectedClass ? 1 : 0),
                         (Result.StatsPerTestCase[tc].PredictedClass ? 1 : 0),
                         TestCase.UseType));


                }

                if (Parameters.ShowTrace && Parameters.Visualize)
                {
                    CGPIndividual.SaveToXml(Ind, Parameters.TraceOutputFolder + "/ind.xml");

                    double MCC = Result.StatsPerTestCase[tc].MCC;
                    //CGPImage ThresholdedOuput = MCC >= 0.00001 ? CGPImageFunctions.Threshold(Output, Ind.Threshold[0]) : CGPImageFunctions.ThresholdInv(Output, Ind.Threshold[0]); ;
                    Image<Gray, byte> BinaryOutput = new Image<Gray, byte>(TestCase.Inputs[0].Img.Width, TestCase.Inputs[0].Img.Height);
                    Image<Bgr, float> ThresholdedOuput = TestCase.Inputs[0].Img.Convert<Bgr, float>();
                    if (ThresholdedOuput.Width != TestCase.Target.Width || ThresholdedOuput.Height != TestCase.Target.Height)
                        ThresholdedOuput = ThresholdedOuput.Resize(TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);



                    for (int x = 0; x < ThresholdedOuput.Width; x++)
                        for (int y = 0; y < ThresholdedOuput.Height; y++)
                        {
                            bool Ignore = TestCase.IgnoreMask != null ? TestCase.IgnoreMask[y, x] : false;
                            if (Ignore)
                            {
                                if (TestCase.IgnoreMask != null && TestCase.IgnoreMask[y, x])
                                {
                                    ThresholdedOuput.Data[y, x, 0] = 128;
                                    ThresholdedOuput.Data[y, x, 1] = 0;
                                    ThresholdedOuput.Data[y, x, 2] = 128;
                                }
                                continue;
                            }

                            if (x < Parameters.ImageBorder || y < Parameters.ImageBorder || x >= ThresholdedOuput.Width - Parameters.ImageBorder || y >= ThresholdedOuput.Height - Parameters.ImageBorder)
                            {
                                ThresholdedOuput.Data[y, x, 0] = 16;
                                ThresholdedOuput.Data[y, x, 1] = 16;
                                ThresholdedOuput.Data[y, x, 2] = 16;

                                continue;
                            }

                            float OriginalV = TestCase.Inputs[0].Img.Data[y, x, 0];
                            bool Expected = TestCase.Target.Img.Data[y, x, 0] > 128f;// this.TargetAsBools[y, x];

                            bool Predicted = CombinedOutput[y, x];

                            if (Predicted)
                                BinaryOutput.Data[y, x, 0] = 255;

                            if (MCC < -0.000001)
                                Predicted = !Predicted;
                            if (Expected && Predicted)
                            {
                                ThresholdedOuput.Data[y, x, 0] = OriginalV;
                                ThresholdedOuput.Data[y, x, 1] = OriginalV + 255;
                                ThresholdedOuput.Data[y, x, 2] = OriginalV;
                            }
                            else if (!Expected && Predicted)
                            {
                                ThresholdedOuput.Data[y, x, 0] = OriginalV + 255;
                                ThresholdedOuput.Data[y, x, 1] = OriginalV;
                                ThresholdedOuput.Data[y, x, 2] = OriginalV;
                            }
                            else if (Expected && !Predicted)
                            {
                                ThresholdedOuput.Data[y, x, 0] = OriginalV;
                                ThresholdedOuput.Data[y, x, 1] = OriginalV;
                                ThresholdedOuput.Data[y, x, 2] = OriginalV + 255;
                            }

                        }

                    if (TestCase.UseType == CGPImageTestCase.TestCaseUseType.Validation)
                    {
                        ThresholdedOuput.Save(String.Format("{0}/ValidationImage_{1:00}.png", Parameters.TraceOutputFolder, tc));
                    }

                    //  ThresholdedOuput.Save(String.Format("{0:000}_{1:0000}.png", v, tc));
                    if (tc < ImageMatrix.ImageMatrixForm.ImageCells.Length)
                    {
                        if (ImageMatrix.ImageMatrixForm.ImageCells[tc] != null)
                            ImageMatrix.ImageMatrixForm.ImageCells[tc].Dispose();

                        //Image<Gray, float>[] Components = new Image<Gray, float>[3];
                        //Components[0] = TestCase.Inputs[0].Img + TestCase.Target.Img;
                        //Components[1] = TestCase.Inputs[0].Img;
                        //Components[2] = TestCase.Inputs[0].Img + ThresholdedOuput.Img;
                        Image<Bgr, float> OverlayImage = ThresholdedOuput;// new Image<Bgr, float>(Components);
                        ImageMatrix.ImageMatrixForm.ImageCells[tc] = OverlayImage.ToBitmap();
                        ImageMatrix.ImageMatrixForm.MCCs[tc] = Result.StatsPerTestCase[tc].MCC;
                        ImageMatrix.ImageMatrixForm.UseType[tc] = TestCase.UseType;
                        ImageMatrix.ImageMatrixForm.CorrectClassification[tc] = Result.StatsPerTestCase[tc].PredictedClass != Result.StatsPerTestCase[tc].ExpectedClass;
                        ImageMatrix.ImageMatrixForm.Refresh();
                        Application.DoEvents();
                    }


                    string BinaryOutputFilename = Parameters.PredictionsOutputFolder + "/" +
                        Path.GetFileNameWithoutExtension(TestCase.InputFileName) + ".png";
                    BinaryOutput.Save(BinaryOutputFilename);
                }

            }

            Result.ComputeFinalStats(Parameters.MaxFilesToToTest);

            if (Parameters.ShowTrace && Parameters.ImageIsClassification)
            {
                CGPImageTestCase.TestCaseUseType[] UseTypes = new CGPImageTestCase.TestCaseUseType[]
                {
                    CGPImageTestCase.TestCaseUseType.Training, 
                    CGPImageTestCase.TestCaseUseType.Validation};

                foreach (CGPImageTestCase.TestCaseUseType UseType in UseTypes)
                {
                    ulong CorrectCount = 0;
                    ulong InCorrectCount = 0;
                    foreach (ConfusionMatrix CM in Result.StatsPerTestCase)
                    {
                        if (CM != null && CM.UseType == UseType)
                        {
                            if (CM.PredictedClass == CM.ExpectedClass)
                                CorrectCount++;
                            else
                                InCorrectCount++;
                        }
                    }

                    Reporting.Say(string.Format(
                        "CLASSIFCATIONRESULT\t{0,12}\t{1,8}\t{2,8}\t{3:000.0000}%",
                        UseType.ToString(),
                        CorrectCount,
                        InCorrectCount,
                        100d * ((double)CorrectCount / (CorrectCount + InCorrectCount))
                        ));
                }

            }

            if (Result.TotalEvaluatedNodeCount == 0)
                Result.Error = double.MaxValue;
            return Result;
        }

        public FitnessFunctionResult TestMap(CGPIndividual Ind, PopulationParameters PopParams)
        {
            EvaluationCount++;
            Ind.EvaluationIndex = EvaluationCount;
            Ind.Evaluated = true;
            FitnessFunctionResult Result = new FitnessFunctionResult();
            Ind.Dirty = false;
            if (Parameters.FitnessScoreType == Parameters.FitnessType.Difference || !Parameters.FitnessIsClassification)
            {
                Result.DifferencePerTestCase = new double[TestSet.TestCases.Count];
                for (int i = 0; i < TestSet.TestCases.Count; i++)
                    Result.DifferencePerTestCase[i] = double.MaxValue;
            }
            else if (Parameters.FitnessScoreType == Parameters.FitnessType.Accuracy || Parameters.FitnessScoreType == Parameters.FitnessType.MCC)
            {
                Result.StatsPerTestCase = new ConfusionMatrix[TestSet.TestCases.Count];
            }


            Result.Error = 0;

            int TotalEvaluatedNodeCount = 0;

            CGPGraphRunner[] Runners = new CGPGraphRunner[Ind.Genotypes.Count];

            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                int EvaluatedNodeCount;
                Runners[g] = new CGPGraphRunner();
                Runners[g].FindNodesToProcess(Ind.Genotypes[g], Parameters.OutputCount, out EvaluatedNodeCount);

                TotalEvaluatedNodeCount += EvaluatedNodeCount;
            }




            Result.Duration = 0;
            Result.TotalEvaluatedNodeCount = TotalEvaluatedNodeCount;

            int TestCasesProcessed = 0;

            TextWriter DataOut = null;

            for (int tc = 0; tc < Parameters.EvaluationMask.Length; tc++)
            {
                TextWriter GraphOutput = null;

                if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                {
                    GraphOutput = new StreamWriter(String.Format("{1}/graph_{0:000000000}.gv", EvaluationCount, Parameters.TraceOutputFolder));

                    GraphOutput.WriteLine("Graph G { ");
                }

                if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                {
                    if (DataOut == null)
                        DataOut = new StreamWriter(String.Format("{0}/data.csv", Parameters.TraceOutputFolder));
                }
                if (Parameters.ShowTrace && tc == 0)
                {
                    for (int g = 0; g < Ind.Genotypes.Count; g++)
                    {
                        if (g == 0)
                            Ind.MixModes[g] = 4;
                        switch (Ind.MixModes[g])
                        {
                            case (0):
                                Reporting.Say("MIXMODE\t" + g + "\tOR");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"OR\"];");
                                }
                                break;
                            case (1):
                                Reporting.Say("MIXMODE\t" + g + "\tAND");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"AND\"];");
                                }
                                break;
                            case (2):
                                Reporting.Say("MIXMODE\t" + g + "\tXOR");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"XOR\"];");
                                }
                                break;
                            case (3):
                                Reporting.Say("MIXMODE\t" + g + "\tNAND");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"NAND\"];");
                                }
                                break;
                            case (4):
                                Reporting.Say("MIXMODE\t" + g + "\tA");
                                if (GraphOutput != null && g > 0)
                                {
                                    GraphOutput.WriteLine("Output_" + g + " -- " + "Output_" + (g - 1) + " [style=bold,color=red,label=\"STOP\"];");
                                }
                                break;
                        }
                    }
                }

                if (!Parameters.EvaluationMask[tc])
                {
                    if (Parameters.ShowTrace && Parameters.Visualize)
                    {
                        if (ImageMatrix.ImageMatrixForm.ImageCells[tc] != null)
                            ImageMatrix.ImageMatrixForm.ImageCells[tc].Dispose();
                        ImageMatrix.ImageMatrixForm.ImageCells[tc] = null;
                    }
                    continue;
                }
                CGPImageTestCase TestCase = TestSet.TestCases[tc];

                if (TestCase.UseType == CGPImageTestCase.TestCaseUseType.Validation && !Parameters.ProcessValidation && Parameters.FitnessIsClassification)
                {
                    Result.StatsPerTestCase[tc] = null;
                    //     Console.Write(".");
                    continue;
                }

                Parameters.DefaultImageHeight = TestCase.Target.Height;
                Parameters.DefaultImageWidth = TestCase.Target.Width;
                Parameters.InputCount = TestCase.Inputs.Count;


                List<CGPImage> Outputs = new List<CGPImage>();
                List<CGPImage> ThresholdedOutputs = new List<CGPImage>();





                for (int g = 0; g < Ind.Genotypes.Count; g++)
                {
                    List<CGPImage> Inputs = new List<CGPImage>();

                    Inputs.AddRange(TestCase.Inputs);

                    if (Outputs.Count > 0)
                    {
                        Inputs.AddRange(Outputs);
                        Inputs.AddRange(ThresholdedOutputs);
                    }

                    if (Parameters.ShowTrace && Parameters.Visualize && tc == 0)
                    {
                        for (int i = 0; i < Inputs.Count; i++)
                        {
                            Inputs[i].Save(String.Format("{2}/inputs_{0:00}_{1:000}.png", g, i, Parameters.TraceOutputFolder));
                        }
                    }

                    Parameters.InputCount = Inputs.Count;
                    Runners[g].Run(Ind.Genotypes[g], Inputs.ToArray(), tc, g.ToString());

                    Runners[g].CleanUp();

                    Result.Duration += Runners[g].Duration;
                    CGPImage Output = Runners[g].GetOutput();


                    if (Output.Width != TestCase.Target.Width || Output.Height != TestCase.Target.Height)
                        Output = CGPImageFunctions.Rescale(Output, TestCase.Target.Width, TestCase.Target.Height);

                    Outputs.Add(Output);

                    if (GraphOutput != null)
                    {
                        GraphOutput.Write(Runners[g].GraphText);
                        GraphOutput.WriteLine();
                    }
                }

                if (GraphOutput != null)
                {
                    GraphOutput.WriteLine("}");
                    GraphOutput.Close();

                    try
                    {
                        ProcessStartInfo PSI = new ProcessStartInfo("dot", "-Tpng " + String.Format("graph_{0:000000000}.gv", EvaluationCount) + " -o " + String.Format("graph_{0:000000000}.png", EvaluationCount));
                        PSI.WorkingDirectory = Parameters.TraceOutputFolder;
                        PSI.UseShellExecute = false;
                        PSI.CreateNoWindow = false;
                        Process.Start(PSI);
                    }
                    catch (Exception e)
                    {

                    }
                }


                float[,] CombinedOutput = new float[TestCase.Target.Height, TestCase.Target.Width];

                double MaxCombined = double.MinValue;
                double MinCombined = double.MaxValue;
                for (int x = 0; x < TestCase.Target.Width; x++)
                    for (int y = 0; y < TestCase.Target.Height; y++)
                        for (int g = 0; g < Ind.Genotypes.Count; g++)
                        {
                            float v = Outputs[g].Img.Data[y, x, 0];
                            if (float.IsNaN(v) || float.IsInfinity(v))
                                v = 0;

                            CombinedOutput[y, x] += v;
                            if (CombinedOutput[y, x] < MinCombined) MinCombined = CombinedOutput[y, x];
                            if (CombinedOutput[y, x] > MaxCombined) MaxCombined = CombinedOutput[y, x];
                        }
                for (int x = 0; x < TestCase.Target.Width; x++)
                    for (int y = 0; y < TestCase.Target.Height; y++)
                        for (int g = 0; g < Ind.Genotypes.Count; g++)
                            CombinedOutput[y, x] /= Ind.Genotypes.Count;

                if (DataOut != null)
                {
                    if (TestCasesProcessed == 0)
                    {
                        DataOut.Write("Index,");
                        for (int i = 0; i < TestCase.Target.Width; i++)
                        {
                            DataOut.Write(i); DataOut.Write(',');
                        }
                        DataOut.WriteLine();
                        DataOut.Write("Target,");
                        for (int i = 0; i < TestCase.Target.Width; i++)
                        {
                            DataOut.Write(TestCase.Target.Img.Data[TestCase.Target.Height / 2, i, 0]);
                            DataOut.Write(',');
                        }
                        DataOut.WriteLine();
                    }

                    DataOut.Write("Input" + tc + "_" + TestCase.UseType + ",");
                    for (int i = 0; i < TestCase.Target.Width; i++)
                    {
                        DataOut.Write(TestCase.Inputs[0].Img.Data[TestCase.Target.Height / 2, i, 0]);
                        DataOut.Write(',');
                    }
                    DataOut.WriteLine();

                    DataOut.Write("Output" + tc + "_" + TestCase.UseType + ",");
                    for (int i = 0; i < TestCase.Target.Width; i++)
                    {
                        DataOut.Write(CombinedOutput[TestCase.Target.Height / 2, i]);
                        DataOut.Write(',');
                    }
                    DataOut.WriteLine();
                }


                //MinCombined = 0;
                //MaxCombined = 255;
                if (Math.Abs(MaxCombined - MinCombined) < 1)
                {
                    MaxCombined++;
                    MinCombined--;
                }
                MaxCombined = Tests.ScantronMaxValue;
                MinCombined = Tests.ScantronMinValue;

                ImageEvaluationCount++;

                double ThisError = TestCase.Compare(CombinedOutput, TestCase.Target.Width, TestCase.Target.Height, Parameters.ImageBorder, 0, 1);
                Result.Error += ThisError;
                Result.DifferencePerTestCase[tc] = ThisError;

                TestCasesProcessed++;
                if (Parameters.ShowTrace && Parameters.Visualize)
                {
                    double Error = Result.Error;
                    //CGPImage ThresholdedOuput = MCC >= 0.00001 ? CGPImageFunctions.Threshold(Output, Ind.Threshold[0]) : CGPImageFunctions.ThresholdInv(Output, Ind.Threshold[0]); ;

                    Image<Bgr, float> VizOuput = TestCase.Inputs[0].Img.Convert<Bgr, float>();
                    if (VizOuput.Width != TestCase.Target.Width || VizOuput.Height != TestCase.Target.Height)
                        VizOuput = VizOuput.Resize(TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                    for (int x = 0; x < VizOuput.Width; x++)
                        for (int y = 0; y < VizOuput.Height; y++)
                        {
                            VizOuput.Data[y, x, 0] = 255f * (float)((CombinedOutput[y, x] - MinCombined) / (MaxCombined - MinCombined));
                            //    VizOuput.Data[y, x, 0] = Math.Abs(VizOuput.Data[y, x, 0] - TestCase.Target.Img.Data[y, x, 0]);
                            VizOuput.Data[y, x, 1] = VizOuput.Data[y, x, 0];
                            VizOuput.Data[y, x, 2] = VizOuput.Data[y, x, 0];
                        }


                    //VizOuput.Save(String.Format("output_{0:000}.png", tc));

                    //TestCase.Target.Save("target.png");
                    if (tc < ImageMatrix.ImageMatrixForm.ImageCells.Length)
                    {
                        if (ImageMatrix.ImageMatrixForm.ImageCells[tc] != null)
                            ImageMatrix.ImageMatrixForm.ImageCells[tc].Dispose();

                        //Image<Gray, float>[] Components = new Image<Gray, float>[3];
                        //Components[0] = TestCase.Inputs[0].Img + TestCase.Target.Img;
                        //Components[1] = TestCase.Inputs[0].Img;
                        //Components[2] = TestCase.Inputs[0].Img + ThresholdedOuput.Img;
                        Image<Bgr, float> OverlayImage = VizOuput;// new Image<Bgr, float>(Components);
                        ImageMatrix.ImageMatrixForm.ImageCells[tc] = OverlayImage.ToBitmap();
                        if (Result.StatsPerTestCase != null)
                            ImageMatrix.ImageMatrixForm.MCCs[tc] = Result.StatsPerTestCase[tc] == null ? 0 : Result.StatsPerTestCase[tc].MCCFast;
                        ImageMatrix.ImageMatrixForm.UseType[tc] = TestCase.UseType;
                        ImageMatrix.ImageMatrixForm.Refresh();
                        Application.DoEvents();
                    }
                }

            }

            if (DataOut != null)
            {
                DataOut.Close();
            }

            Result.Error /= TestCasesProcessed;

            if (Parameters.FitnessAggregationType == Parameters.FitnessAggregation.WORST)
            {
                Result.Error = double.MaxValue;
                for (int i = 0; i < Result.DifferencePerTestCase.Length; i++)

                    if (Result.DifferencePerTestCase[i] < Result.Error)
                        Result.Error = Result.DifferencePerTestCase[i];
            }

            //Result.Error = Math.Sqrt(Result.Error);
            //Result.ComputeFinalStats(Parameters.MaxFilesToToTest);
            if (double.IsInfinity(Result.Error) || double.IsNaN(Result.Error))
                Result.Error = Double.MaxValue;
            return Result;
        }
    }
}