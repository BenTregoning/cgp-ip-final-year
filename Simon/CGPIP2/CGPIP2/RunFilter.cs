﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGPIP2
{
    public class RunFilter
    {
        public static CGPIndividual Ind = null;

        public static void LoadIndividual()
        {
            Reporting.TextFile = null;
           Parameters.LoadParameters("parameters.ini");
            //Ind = Tests.PickBestIndividualInFolder("C:\\work\\csharp\\CGPIP2\\CGPIP2\\bin\\x64\\Release\\rr001\\Server\\");
            Ind = Tests.PickBestIndividualInFolder("C:\\Users\\simonh\\Google Drive\\Scantron\\");
        }

        public static Image<Bgr, float> Run(Image<Bgr, float> SrcImg, out Image<Gray, byte> BinaryOutput, bool InvertOutput)
        {
            Parameters.ShowTrace = true;
            Parameters.Visualize = true;
            
            CGPImageTestCase TestCase = new CGPImageTestCase(SrcImg, Parameters.ScaleInputs, Parameters.SplitBGR, Parameters.SplitHSB, -1);
            TestCase.Target = TestCase.Inputs[0];
            CGPGraphRunner[] Runners = new CGPGraphRunner[Ind.Genotypes.Count];
            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                int EvaluatedNodeCount;
                Runners[g] = new CGPGraphRunner();
                Runners[g].FindNodesToProcess(Ind.Genotypes[g], Parameters.OutputCount, out EvaluatedNodeCount);
            }

            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                Ind.Thresholds[g] = 128;
                if (g == 0)
                    Ind.MixModes[g] = 4;
            }


            Parameters.DefaultImageHeight = TestCase.Target.Height;
            Parameters.DefaultImageWidth = TestCase.Target.Width;
            Parameters.InputCount = TestCase.Inputs.Count;


            List<CGPImage> Outputs = new List<CGPImage>();
            List<CGPImage> ThresholdedOutputs = new List<CGPImage>();


            for (int g = 0; g < Ind.Genotypes.Count; g++)
            {
                List<CGPImage> Inputs = new List<CGPImage>();

                Inputs.AddRange(TestCase.Inputs);

                if (Outputs.Count > 0)
                {
                    Inputs.AddRange(Outputs);
                    Inputs.AddRange(ThresholdedOutputs);
                }


                Parameters.InputCount = Inputs.Count;
                Runners[g].Run(Ind.Genotypes[g], Inputs.ToArray(), 0, g.ToString());

                Runners[g].CleanUp();

                CGPImage Output = Runners[g].GetOutput();
                if (Parameters.MedianFilterOutput > 0)
                    Output = CGPImageFunctions.SmoothMedian(Output, Parameters.MedianFilterOutput);
                //                    if (Parameters.ShowTrace && Parameters.Visualize)
                CGPImage ThresholdedOutput = CGPImageFunctions.Threshold(Output, Ind.Thresholds[g]);

                if (ThresholdedOutput.Width != TestCase.Target.Width || ThresholdedOutput.Height != TestCase.Target.Height)
                    ThresholdedOutput = CGPImageFunctions.Rescale(ThresholdedOutput, TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_NN);


                ThresholdedOutputs.Add(ThresholdedOutput);

                if (Output.Width != TestCase.Target.Width || Output.Height != TestCase.Target.Height)
                    Output = CGPImageFunctions.Rescale(Output, TestCase.Target.Width, TestCase.Target.Height);

                Outputs.Add(Output);
            }


            bool[,] CombinedOutput = new bool[TestCase.Target.Height, TestCase.Target.Width];


            for (int x = 0; x < TestCase.Target.Width; x++)
                for (int y = 0; y < TestCase.Target.Height; y++)
                    for (int g = 0; g < Ind.Genotypes.Count; g++)
                    {
                        float v = Outputs[g].Img.Data[y, x, 0];
                        if (float.IsNaN(v) || float.IsInfinity(v))
                            v = 0;

                        bool SingleOutput = v > Ind.Thresholds[g];
                        if (g == 0)
                            Ind.MixModes[g] = 4;
                        switch (Ind.MixModes[g])
                        {
                            case (0):
                                CombinedOutput[y, x] = CombinedOutput[y, x] | SingleOutput;
                                break;
                            case (1):
                                CombinedOutput[y, x] = CombinedOutput[y, x] & SingleOutput;
                                break;
                            case (2):
                                CombinedOutput[y, x] = CombinedOutput[y, x] ^ SingleOutput;
                                break;
                            case (3):
                                CombinedOutput[y, x] = !(CombinedOutput[y, x] & SingleOutput);
                                break;
                            case (4):
                                CombinedOutput[y, x] = SingleOutput;
                                break;
                        }

                    }





            double MCC = 1;
            //CGPImage ThresholdedOuput = MCC >= 0.00001 ? CGPImageFunctions.Threshold(Output, Ind.Threshold[0]) : CGPImageFunctions.ThresholdInv(Output, Ind.Threshold[0]); ;
           BinaryOutput = new Image<Gray, byte>(TestCase.Inputs[0].Img.Width, TestCase.Inputs[0].Img.Height);
            Image<Bgr, float> ThresholdedOuput = TestCase.Inputs[0].Img.Mul(0.5).Convert<Bgr, float>();
            if (ThresholdedOuput.Width != TestCase.Target.Width || ThresholdedOuput.Height != TestCase.Target.Height)
                ThresholdedOuput = ThresholdedOuput.Resize(TestCase.Target.Width, TestCase.Target.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

            for (int x = 0; x < ThresholdedOuput.Width; x++)
                for (int y = 0; y < ThresholdedOuput.Height; y++)
                {


                    float OriginalV = TestCase.Inputs[0].Img.Data[y, x, 0];

                    bool Predicted = CombinedOutput[y, x];
                    if (InvertOutput) Predicted = !Predicted;

                    if (Predicted)
                        BinaryOutput.Data[y, x, 0] = 255;

                    if (Predicted)
                    {
                        ThresholdedOuput.Data[y, x, 0] = OriginalV;
                        ThresholdedOuput.Data[y, x, 1] = OriginalV;
                        ThresholdedOuput.Data[y, x, 2] = 255;
                    }

                }

            //    Video.WriteFrame(ThresholdedOuput.Convert<Bgr, byte>());
            return ThresholdedOuput;
        }

        public static List<Rectangle> Blobs(Image<Gray, float> A, int DiscardSizeMin, bool FixOverlaps, double ScaleBlobs, bool DoNotAddIfTouchingEdges)
        {
            List<Rectangle> B = new List<Rectangle>();

            using (MemStorage storage = new MemStorage())
            {
                for (Contour<Point> contours = A.Convert<Gray, byte>().FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);
                    Rectangle BB = currentContour.BoundingRectangle;

                    if (DiscardSizeMin > 1 && (BB.Width < DiscardSizeMin || BB.Height < DiscardSizeMin))
                        continue;

                    int EdgeWidth = 10;
                    if (DoNotAddIfTouchingEdges &&
                        (BB.X <= EdgeWidth || BB.Y <= EdgeWidth || BB.Right >= A.Width - EdgeWidth || BB.Bottom >= A.Height - EdgeWidth))
                        continue;

                    if (ScaleBlobs > 0)
                    {
                        double ScaleROI = ScaleBlobs;
                        int NewWidth = (int)(ScaleROI * BB.Width);
                        int NewHeight = (int)(ScaleROI * BB.Height);
                        int NewX = BB.X - ((NewWidth - BB.Width) / 2);
                        int NewY = BB.Y - ((NewHeight - BB.Height) / 2);
                        if (NewX < 0) NewX = 0;
                        if (NewY < 0) NewY = 0;
                        if (NewX + NewWidth >= A.Width)
                            NewWidth = A.Width - 1 - NewX;
                        if (NewY + NewHeight >= A.Height)
                            NewHeight = A.Height - 1 - NewY;
                        B.Add(new Rectangle(NewX, NewY, NewWidth, NewHeight));
                    }
                    else
                    {
                        B.Add(BB);
                    }
                }
            }

            if (FixOverlaps)
            {
                for (int i = 0; i < B.Count; i++)
                    for (int j = 0; j < B.Count; j++)
                    {
                        if (i == j)
                            continue;
                        if (B[i].IntersectsWith(B[j]))
                        {
                            Rectangle N = new Rectangle(
                                Math.Min(B[i].X, B[j].X),
                                Math.Min(B[i].Y, B[j].Y),
                                Math.Max(B[i].Right, B[j].Right) - Math.Min(B[i].X, B[j].X),
                                Math.Max(B[i].Bottom, B[j].Bottom) - Math.Min(B[i].Y, B[j].Y));
                            B.RemoveAt(i);
                            if (j > i) B.RemoveAt(j - 1); else B.RemoveAt(j);
                            i = 0; j = 0;
                            B.Add(N);
                        }
                    }
            }

            return B;
        }


        public static void Test()
        {
            LoadIndividual();
            string InputFolder = "C:\\work\\MI\\EvoScan1D\\EvoScan1D\\bin\\x64\\Release\\demoImages\\";
            string OutputFolder = InputFolder + "\\Detections\\";
            if (!Directory.Exists(OutputFolder))
                Directory.CreateDirectory(OutputFolder);
            string[] Files = Directory.GetFiles(InputFolder, "*.png");

            TextWriter Report = new StreamWriter("cookiesreport.txt");

            foreach (string F in Files)
            {
                
                Image<Bgr, float> Img = new Image<Bgr, float>(F);
                Image<Gray, byte> Bin = new Image<Gray, byte>(Img.Width, Img.Height);
                Image<Bgr, float> Pred = Run(Img, out Bin, true);
                List<Rectangle> FoundBlobs = Blobs(Bin.Convert<Gray,float>(), 1, false, -1, false);
                Report.WriteLine(F+"\t"+FoundBlobs.Count());
                foreach (Rectangle R in FoundBlobs)
                {
                    Rectangle R2 = new Rectangle(R.X * 4, R.Y * 4, R.Width * 4, R.Height * 4);
                    Img.Draw(R2, new Bgr(Color.Blue), 4);
                    Report.Write("\t");

                    Report.Write(R2.X + "," + R2.Y + "  " + R2.Width + "x" + R2.Height + " ");
                    Bin.ROI = R;
                    Gray S = Bin.GetSum();
                    Report.Write(String.Format("{0:000.0}", (100d*(S.Intensity/255)) / (R.Width * R.Height)));
                    Report.WriteLine();
                }

                Img.Save(OutputFolder + "\\" + Path.GetFileName(F));
                Report.Flush();
            }

            Report.Close();
        }
    }
}
