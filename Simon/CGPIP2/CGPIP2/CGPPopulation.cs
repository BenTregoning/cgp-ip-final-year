﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGPIP2
{
   public class CGPPopulation
   {
      List<CGPIndividual> Individuals = new List<CGPIndividual>();
      public PopulationParameters Parameters = new PopulationParameters();

      public double StartFitness = double.MaxValue;
      public ulong StartFitnessEvaluations = 0;

      public void ResetFitnesses()
      {
          foreach (CGPIndividual Ind in Individuals)
          {
              Ind.Fitness = null;
              Ind.Evaluated = false;
          }
      }

       public void MarkLearningRatePoint()
      {
          this.Sort();
          StartFitnessEvaluations = FitnessFunction.EvaluationCount;
          StartFitness = this.Individuals[0].Fitness.Error;
      }

       public double LearningRate()
       {
           this.Sort();
           double Current = this.Individuals[0].Fitness.Error;
           ulong Evals = FitnessFunction.EvaluationCount - StartFitnessEvaluations;
           Reporting.Say("Current:" + Current + " Evals:" + Evals);
           return (StartFitness - Current) / Evals;
       }

      public int Count
      {
         get { return this.Individuals.Count; }
      }

      public CGPIndividual this[int Index]
      {
         get { return this.Individuals[Index]; }
         set { this.Individuals[Index] = value; }
      }

      public void Add(CGPIndividual Ind)
      {
         this.Individuals.Add(Ind);
      }

      public void Clear()
      {
         this.Individuals.Clear();
      }

      public void Sort()
      {
         this.Individuals.Sort();
       //  for (int i = 0; i < this.Individuals.Count; i++)
         //    Reporting.Say(i + "\t" + this.Individuals[i].Fitness.BestError + "\t" + this.Individuals[i].EvaluationIndex);
      }
       
       public CGPIndividual Tournament(int Size)
      {
          CGPIndividual Best = this.Individuals[FastRandom.Rng.Next(0, this.Individuals.Count)];
          for (int i = 0; i < Size; i++)
          {
              CGPIndividual Candidate = this.Individuals[FastRandom.Rng.Next(0, this.Individuals.Count)];
              if (Candidate < Best)
                  Best = Candidate;
          }
          return Best;
      }

       public void MakeNextWithFront(int PopulationSize)
       {
           this.Individuals.Sort();
           SortedDictionary<int, CGPIndividual> Front = new SortedDictionary<int, CGPIndividual>();
           foreach (CGPIndividual Ind in this.Individuals)
           {
               if (Front.ContainsKey(Ind.Fitness.TotalEvaluatedNodeCount))
               {
                   if (Ind.Fitness.Error < Front[Ind.Fitness.TotalEvaluatedNodeCount].Fitness.Error)
                   {
                       Front[Ind.Fitness.TotalEvaluatedNodeCount] = Ind;
                   }
               }
               else
               {
                   Front.Add(Ind.Fitness.TotalEvaluatedNodeCount, Ind);
               }
           }

           List<CGPIndividual> NewIndividuals = new List<CGPIndividual>();
           int[] Lengths = Front.Keys.ToArray();
           NewIndividuals.Add(this.Individuals[0]);
           foreach (int L in Lengths)
           {
               ulong Age = Front[L].EvaluationIndex - FitnessFunction.EvaluationCount;
               if (Age<100)
                NewIndividuals.Add(Front[L]);
           }

           while (NewIndividuals.Count < PopulationSize)
           {
               int L = Lengths[FastRandom.Rng.Next(0, Lengths.Length)];
               CGPIndividual Child = Front[L].Clone();
               Child.Mutate2(this.Parameters, true);
               NewIndividuals.Add(Child);
           }

           this.Individuals = NewIndividuals;
           this.Individuals.Sort();
       }

      public void MakeNext(int PopulationSize)
      {
         this.Individuals.Sort();

         List<CGPIndividual> NewIndividuals = new List<CGPIndividual>();

         CGPIndividual Parent = this.Individuals[0];
         
         NewIndividuals.Add(Parent);

         bool AllowIncreaseInGenotypeCount = (Parent.EvaluationIndex - CGPIP2.Parameters.LastFitnessImprovement) > CGPIP2.Parameters.EvaluationTimeout;

         while (NewIndividuals.Count < PopulationSize)
         {
            CGPIndividual Child = Parent.Clone();
            Child.ParentFitness = Parent.Fitness.Clone();
            Child.Mutate2(this.Parameters, AllowIncreaseInGenotypeCount || Parent.Fitness.Error < CGPIP2.Parameters.IncreaseThreshold );
            NewIndividuals.Add(Child);
         }
          /*
         if (!CGPIP2.Parameters.EvolveParametersOnly)
             while (NewIndividuals.Count < PopulationSize * 2)
             {

                 CGPIndividual ParentA = Tournament(3);
                 CGPIndividual ParentB = Tournament(3);
                 
                 AllowIncreaseInGenotypeCount=AllowIncreaseInGenotypeCount || (ParentA.Fitness.Error < CGPIP2.Parameters.IncreaseThreshold * 2) ||
                     (ParentB.Fitness.Error < CGPIP2.Parameters.IncreaseThreshold * 2);

                 List<CGPIndividual> Children = ParentA.Crossover(ParentB);
                 
                 Children[0].Mutate(this.Parameters, AllowIncreaseInGenotypeCount);
                 Children[1].Mutate(this.Parameters, AllowIncreaseInGenotypeCount);
                 NewIndividuals.AddRange(Children);
             }
         */
         this.Individuals = NewIndividuals;
      }
   }
}